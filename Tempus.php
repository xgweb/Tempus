<?php
namespace XGWeb;
use DateTime, DateTimeZone;

/**
 * Date/Time Utilities
 *
 * @author Rob Steiner
 * @copyright  2016 Project Ana, Inc.
 *
 * Copyright (C) Project Ana, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Rob Steiner <rob@projectana.com>, February 2016
 */
class Tempus extends DateTime {

    public static function instance($time = null, $timezone = null)
    {
        if (is_numeric($time)) {
            $time = date('Y-m-d H:i:s', $time);
        }

        return new self($time, $timezone);
    }

    public static function convert(DateTime $date) {
        return self::instance($date->format('U'));
    }

    public function mysql()
    {
        return $this->format('Y-m-d H:i:s');
    }

    public function since($differenceFormat = '%a' )
    {
        $datetime1 = date_create($date_1);
        $datetime2 = date_create($date_2);

        $interval = date_diff($this, new DateTime());

        return $interval->format($differenceFormat);
    }

    public function timezoneName()
    {
        $offset = $this->format('T') - 5;
        $offset *= 3600;
        $abbrarray = timezone_abbreviations_list();
        foreach ($abbrarray as $abbr)
        {
            foreach ($abbr as $city) {
                if ($city['offset'] == $offset) {
                        return $city['timezone_id'];
                }
            }
        }

        return FALSE;
    }

    public static function isBetween(self $begin, self $end, self $now = null): bool
    {
        $now = $now ?: self::instance();
        if ($now->getTimestamp() >= $begin->getTimestamp() && $now->getTimestamp() <= $end->getTimestamp()) {
            return true;
        }

        return false;
    }

    public static function getNicePastDate(self $date)
    {
        $today     = self::instance();
        $yesterday = self::instance()->sub(new DateInterval('P1D'));
        $oneweek   = self::instance()->sub(new DateInterval('P6D'));
        $onemonth  = self::instance()->modify('-1 month');
        $oneyear  = self::instance()->modify('-1 year');

        if ($date->getTimestamp() > $today->getTimestamp()) {
            return self::getNiceDate($date);
        }

        if ($date->format('Ymd') === $today->format('Ymd')) {
            return 'today';
        }

        if ($date->format('Ymd') === $yesterday->format('Ymd')) {
            return 'yesterday ';
        }

        if(self::isBetween($oneweek, $today, $date)) {
            return 'on ' . $date->format('l') ;
        }

        if(self::isBetween($onemonth, $today, $date)) {
            $weeks = strval(round($today->diff($date)->days / 7));
            return $weeks . ' week' . ($weeks == 1 ? '' : 's') . ' ago';
        }

        if(self::isBetween($oneyear, $today, $date)) {
            $months = strval(round($today->diff($date)->days / 30));
            return $months . ' month' . ($months == 1 ? '' : 's') . ' ago';
        }

        $years = strval(round($today->diff($date)->days / 365));
        return $years . ' year' . ($years == 1 ? '' : 's') . ' ago';
    }

    public static function getNiceDate(self $date)
    {
        $today     = self::instance();
        $yesterday = self::instance()->sub(new DateInterval('P1D'));
        $oneweek   = self::instance()->modify('-6 days');

        $date->setTimeZone(new DateTimeZone('America/New_York'));

        if ($date->format('Ymd') === $today->format('Ymd')) {
            return 'today at ' .  $date->format('g:ia');
        }
        else if ($date->format('Ymd') === $yesterday->format('Ymd')) {
            return 'yesterday at ' .  $date->format('g:ia');
        }
        else if(self::isBetween($oneweek, $today, $date)) {
            return 'on ' . $date->format('l') . ' at ' .  $date->format('g:ia');
        }
        else {
            return 'on ' . $date->format('F j, Y');
        }

    }

    public function sub($interval)
    {
        parent::sub(date_interval_create_from_date_string($interval));
        return $this;
    }

    public function add($interval)
    {
        parent::add(date_interval_create_from_date_string($interval));
        return $this;
    }
    
    public function getMicrotimeStamp(): int
    {
        return $this->getTimestamp() * 1000;
    }

    public static function daysUntil(self $date, int $days)
    {
        return $date->diff(new \DateTime(date('Y-m-d h:i:s', strtotime('+' . ($days + 1) . ' days'))))
            ->format('%d');
    }

    function __toString()
    {
        return $this->mysql();
    }

}
